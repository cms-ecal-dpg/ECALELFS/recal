.. rECAL documentation master file, created by
   sphinx-quickstart on Thu May 14 15:08:11 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to rECAL's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Docs
====

.. doxygenclass:: recal::ICManager
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
