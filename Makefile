CXX = g++
CXXFLAGS = -std=c++17 -fPIC
SOFLAGS = -shared -O3 
INCLUDE = -I"./"
PYINCLUDE := `python3 -m pybind11 --includes`
PYEXT := `python3-config --extension-suffix`
PYBINDLIB = lib/recal$(shell python3-config --extension-suffix)
PYSITE := `python -c 'import site; print(site.getsitepackages()[0])'`
LIB = -L"./lib/"

all: setup lib/libICManager.so lib/ICManager.o bin/example ${PYBINDLIB}

.PHONY: setup
setup:	
	@$ if [ ! -f interface/json.hpp ]; \
	then echo "Downloading JSON for Modern C++ https://nlohmann.github.io/json/ latest version"; \
	curl -s https://api.github.com/repos/nlohmann/json/releases/latest | grep -e 'browser_download_url.*hpp"' | cut -d '"' -f 4 | wget -P interface/ -qi -; fi

lib/%.o : src/%.cc interface/ICManager.h interface/json.hpp
	@echo " CXX $<"
	@$ $(CXX) $(CXXFLAGS) -c -o $@ $< $(INCLUDE) 

${PYBINDLIB}: src/python_bindings.cc src/ICManager.cc interface/ICManager.h interface/json.hpp
	@echo " Python bindings"
	@$ $(CXX) $(CXXFLAGS) $(SOFLAGS) -o $@ $^ $(INCLUDE) $(PYINCLUDE)

lib/libICManager.so: lib/ICManager.o
	@echo " CXX $<"
	@$ $(CXX) $(CXXFLAGS) $(SOFLAGS) -o $@ $^ $(INCLUDE) $(LIB)

bin/%: test/%.cpp lib/libICManager.so
	@echo " CXX $<"
	@$ $(CXX) $(CXXFLAGS) -o $@ $^ $(INCLUDE) $(LIB)

.PHONY: install
install:
	@$ mkdir -p ${PYSITE}/recal
	@$ echo 'from . import recal' > ${PYSITE}/recal/__init__.py
	@$ echo 'from .recal import ICManager' >> ${PYSITE}/recal/__init__.py
	@$ cp ${PYBINDLIB} ${PYSITE}/recal
	@$ cp scripts/*py ${VIRTUAL_ENV}/bin/

.PHONY: clean
clean:
	rm -f lib/*
	rm -f bin/*

.PHONY: distclean
distclean: clean
	rm interface/json.hpp

.PHONY: docs
docs:
	@echo " Generate documentation"
	@$ cd docs && doxygen && make html

.PHONY: docsclean
docsclean: 
	rm -rf docs/build
	rm -rf docs/xml
