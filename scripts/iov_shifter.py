#!/usr/bin/env python3

import argparse
import os
import json
from copy import deepcopy

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Shift IOV in IC file.')
    parser.add_argument('-i', '--ic-conf', dest='ics', type=str, help='IC summary file')
    parser.add_argument('-o', '--output', dest='out_file', type=str, help='Output file name')
    parser.add_argument('-n', dest='niovs', type=int, help='Number of IOV shifts')

    options = parser.parse_args()
    
    options.ics = os.path.abspath(options.ics)
    options.out_file = os.path.abspath(options.out_file)

    
    with open(options.ics, 'r') as ics:
        original = json.load(ics)

    original = {k: v for k, v in sorted(original['IOVs'].items(), key=lambda item: item[1]['begin'])}
    shifted = {'IOVs' : {}}

    for i in range(options.niovs):
        shifted['IOVs']['IOV'+str(i)] = deepcopy(original['IOV1'])
        shifted['IOVs']['IOV'+str(i)]['begin'] = original['IOV'+str(i)]

    iovs = list(original.keys())
    for i,iov in enumerate(iovs[options.niovs:]):
        shifted['IOVs'][iov] = deepcopy(original[iov])
        shifted['IOVs'][iov]['file'] = original[iovs[i]]['file']
    
    with open(options.out_file, 'w') as outfile:
        json.dump(shifted, outfile, indent=4)
