#!/usr/bin/env python3

import argparse
import os
import json
import glob
import shutil
import subprocess

import ecalautoctrl as ectrl

from multiprocessing.dummy import Pool as ThreadPool

def merge_outputs(main_file, out_prefix):
    main_dir = os.path.dirname(main_file)
    main_file = os.path.basename(main_file)
    files = main_dir+'/unmerged/'+out_prefix+'-*root'
    os.system('hadd %s/%s-%s %s' % (main_dir, out_prefix, main_file, files))

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Submit recalefl.py jobs.')
    parser.add_argument('-C', '--elf-campaign', dest='elf_campaign', type=str, help='ECALElf campaign')
    parser.add_argument('-e', '--era', dest='era', type=str, help='ECALElf era')
    parser.add_argument('-c', '--campaign', dest='campaign', type=str, help='Output campaign')
    parser.add_argument('-i', '--ic-conf', dest='ics', type=str, help='IC summary file')
    parser.add_argument('-o', '--output', dest='output', type=str, help='Output file prefix')
    parser.add_argument('-b', '--batch', dest='batch', type=str, help='Batch system: condor or threads')
    parser.add_argument('-q', '--queue', dest='queue', type=str, help='Condor job flavour')
    parser.add_argument('--n-threads', dest='n_threads', type=int, default=10, help='Number of parallel threads')
    parser.add_argument('--max-retries', dest='max_retries', default=3, type=int, help='Maximum number of retries per job')
    parser.add_argument('--merge', dest='merge', action='store_true', default=False, help='Merge output files')
    parser.add_argument('--dryrun', dest='dryrun', action='store_true', default=False, help='Create jobs but do not submit them')

    options = parser.parse_args()

    options.ics = os.path.abspath(options.ics)
    options.cwd = os.path.abspath(options.campaign)

    if options.merge:
        merge_outputs(list(summary.values())[0]['mergedFiles']['main'], options.output)
        exit(0)

    os.makedirs(options.cwd, exist_ok=True)
    #shutil.copyfile(os.environ['RECAL_BASE']+'/scripts/recalelf.py', options.cwd+'/recalelf.py')

    dctrl = ectrl.DatasetCtrl(campaign=options.elf_campaign, era=options.era, workflow='ECALElf')
    n_elf_files = len([f for f in dctrl.getAllFiles() if f['type']=='main'])

    ### jobs script
    with open(options.cwd+'/recal_job.sh', 'w') as job:
        job.write("#!/bin/bash \n\n")
        job.write("mkdir output \n") 
        job.write("source /eos/project-c/cms-ecal-calibration/ecal-venv/bin/activate \n\n")
        job.write("ecalautomation.py -c %s -w rECAL -e %s jobctrl --id ${1} --running \n\n" % (options.campaign, options.era))
        job.write("recalelf.py -i %s --elf-campaign %s -e %s -w ECALElf -c %s --id ${1} \n\n" % 
                  (options.ics, options.elf_campaign, options.era, options.campaign))
        job.write("RETRECAL=$? \n")
        job.write("""
        if [ "$RETRECAL\" == "0" ]
        then
            cp output/*root ${2}
            OFILE=`ls output/*root`
            OFILE=`basename $OFILE`
            RETCOPY=$?
        else
            RETCOPY=1
        fi
        """)
        job.write("RET=$(echo \"$RETRECAL+$RETCOPY\" | bc) \n")        
        job.write("""
        if [ "$RET\" == "0" ]
        then
            ecalautomation.py -c %s -w rECAL -e %s jobctrl --id ${1} --done
            ecalautomation.py -c %s -w rECAL -e %s setfile --id ${1} --path ${2}/$OFILE
        else
            ecalautomation.py -c %s -w rECAL -e %s jobctrl --id ${1} --failed
        fi\n""" % (options.campaign, options.era, options.campaign, options.era, options.campaign, options.era))
        job.write("exit $RET \n")
        
    # ### DAG file
    # with open(options.cwd+'/recal_job.dag', 'w') as job:
    #     job.write("JOB %s %s \n\n" % (list(summary.keys())[0], options.cwd+'/recal_job.sub'))
    #     job.write("Retry %s 2 \n" % (list(summary.keys())[0]))

    ### batch submission (either condor or parallel threads)
    if options.batch == 'condor':
        with open(options.cwd+'/recal_job.sub', 'w') as sub:
            sub.write('+JobFlavour    = "%s" \n\n' % options.queue)
            sub.write('executable     = %s \n' % (options.cwd+'/recal_job.sh'))
            sub.write('arguments      = $(ProcId) %s \n' % options.output)
            sub.write('output         = %s/recallog.$(ClusterId).$(ProcId).out \n' % options.cwd)
            sub.write('error          = %s/recallog.$(ClusterId).$(ProcId).err \n' % options.cwd)
            sub.write('log            = %s/recallog.$(ClusterId).log \n\n' % options.cwd)
            sub.write('on_exit_remove = (ExitBySignal == False) && (ExitCode == 0) \n')
            sub.write('max_retries    = 3 \n')
            sub.write('queue %d \n' % n_elf_files)

        if not options.dryrun:
            ret = subprocess.run(['ecalautomation.py', '-c', str(options.campaign), '-e', str(options.era), '-w', 'rECAL', 'submit', '-n', str(n_elf_files)])
            subprocess.run(['condor_submit', options.cwd+'/recal_job.sub'])

    elif options.batch == 'threads' and not options.dryrun:
        def exec_recal(i):
            args = ['bash', options.cwd+'/recal_job.sh', str(i)]
            ret = subprocess.run(args, capture_output=True)
            return ret

        retries = 0
        jobs = range(len(main_files))

        print("Running %d parallel jobs..." % len(jobs))
        while len(jobs)>0 and retries<options.max_retries:
            retries += 1

            pool = ThreadPool(options.n_threads)
            res = pool.map(exec_recal, jobs)

            pool.close()
            pool.join()

            ### print output for failed jobs and reschedule
            jobs = []
            for r in res:
                if r.returncode != 0:
                    print(r)
                    print("Rescheduling job #"+r.args[-1])
                    jobs.append(r.args[-1])
                    
        if len(jobs) == 0:
            print("All jobs successfully completed")
        else:
            print("Jobs %s failed %d tries" % (','.join([str(j) for j in jobs]), retries))
        
