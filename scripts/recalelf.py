#!/usr/bin/env python3

import argparse
import os
import sys

from math import sqrt
from functools import reduce
from array import array

import ROOT
import recal
import ecalautoctrl as ectrl

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Update energy calibration in existing ECALElf ntuples.')
    parser.add_argument('--egm', dest='egm_tree', type=str, help='main ECALElf tree file')
    parser.add_argument('--extra', dest='extra_tree', type=str, help='ECALElf extra calib tree file')
    parser.add_argument('-i', '--ic-conf', dest='ics', type=str, help='IC summary file')
    parser.add_argument('-o', '--output', dest='output', default='output/', type=str, help='Output file name')
    parser.add_argument('-c', '--campaign', dest='campaign', default=None, type=str, help='New campaign name (upload info to influxdb)')
    parser.add_argument('--elf-campaign', dest='elf_campaign', default=None, type=str, help='ECALElf input campaign name (fetch inputs from influxdb)')
    parser.add_argument('-e', '--era', dest='era', default=None, type=str, help='Era name (fetch inputs from influxdb)')
    parser.add_argument('-w', '--workflow', dest='workflow', default=None, type=str, help='Workflow name (fetch inputs from influxdb)')
    parser.add_argument('--id', dest='id', default=None, type=str, help='Id of the file to be processed (fetch inputs from influxdb)')

    options = parser.parse_args()
    
    if options.elf_campaign and options.workflow and options.id:
        dctrl = ectrl.DatasetCtrl(campaign=options.elf_campaign, era=options.era, workflow=options.workflow)
        options.egm_tree = dctrl.getFilesIdRange(start_id=options.id, n_files=1, type='main')[-1]['path'] 
        options.extra_tree = dctrl.getFilesIdRange(start_id=options.id, n_files=1, type='extraCalib')[-1]['path'] 
        options.output += os.path.basename(options.egm_tree).replace('ntuple', options.campaign)
        
    os.makedirs(os.path.dirname(options.output), exist_ok=True)

    options.ics = os.path.abspath(options.ics)

    main_file = ROOT.TFile.Open(options.egm_tree)
    main_tree = main_file.Get('selected')    
    main_tree.BuildIndex("runNumber", "eventNumber")
    rechit_file = ROOT.TFile.Open(options.extra_tree)
    rechit_tree = rechit_file.Get('extraCalibTree')
    rechit_tree.BuildIndex("runNumber", "eventNumber")
    main_tree.AddFriend(rechit_tree)
    
    recal_file = ROOT.TFile.Open(options.output, 'RECREATE')
    recal_tree = ROOT.TTree('recal_tree', 'Recalibrated with %s' % options.ics)
    
    runNumber = array('i', [0])
    lumiBlock = array('h', [0])
    eventNumber = array('l', [0])
    energyRecHitSCEle1_recal = ROOT.std.vector('float')()
    energyRecHitSCEle2_recal = ROOT.std.vector('float')()
    rawEnergySCEle_recal = array('f', [0., 0., 0.])
    mustEnergySCEle_recal = array('f', [0., 0., 0.])
    invMass_ECAL_ele_recal = array('f', [0.])
    recal_tree.Branch('runNumber', runNumber, 'runNumber/i')
    recal_tree.Branch('lumiBlock', lumiBlock, 'lumiBlock/s')    
    recal_tree.Branch('eventNumber', eventNumber, 'eventNumber/l')
    recal_tree.Branch('energyRecHitSCEle1_recal', energyRecHitSCEle1_recal)
    recal_tree.Branch('energyRecHitSCEle2_recal', energyRecHitSCEle2_recal)
    recal_tree.Branch('rawEnergySCEle_recal', rawEnergySCEle_recal, 'rawEnergySCEle_recal[3]/F')
    recal_tree.Branch('mustEnergySCEle_recal', mustEnergySCEle_recal, 'mustEnergySCEle_recal[3]/F')
    recal_tree.Branch('invMass_ECAL_ele_recal', invMass_ECAL_ele_recal, 'invMass_ECAL_ele_recal/F')
    
    icman = recal.ICManager(options.ics)    

    for evt in main_tree:
        energyRecHitSCEle1_recal.clear()
        energyRecHitSCEle2_recal.clear()

        for i, e in enumerate(main_tree.energyRecHitSCEle1):
            energyRecHitSCEle1_recal.push_back(e*icman.getIC(main_tree.XRecHitSCEle1[i],
                                                            main_tree.YRecHitSCEle1[i],
                                                            main_tree.ZRecHitSCEle1[i],
                                                            main_tree.runNumber))
        ### compute the recalected raw energy as sum of the rec hit energies (taking into account the fraction assigned to this SC).
        if energyRecHitSCEle1_recal.size() > 0:
            rawEnergySCEle_recal[0] = reduce(lambda x,y: x+y, list(map(lambda e, f : e*f, energyRecHitSCEle1_recal, main_tree.fracRecHitSCEle1)))
        mustEnergySCEle_recal[0] = main_tree.mustEnergySCEle[0]*(rawEnergySCEle_recal[0]/main_tree.rawEnergySCEle[0])

        for i, e in enumerate(main_tree.energyRecHitSCEle2):
            energyRecHitSCEle2_recal.push_back(e*icman.getIC(main_tree.XRecHitSCEle2[i],
                                                            main_tree.YRecHitSCEle2[i],
                                                            main_tree.ZRecHitSCEle2[i],
                                                            main_tree.runNumber))
        ### compute the recalected raw energy as sum of the rec hit energies (taking into account the fraction assigned to this SC).
        if energyRecHitSCEle2_recal.size() > 0:
            rawEnergySCEle_recal[1] = reduce(lambda x,y: x+y, list(map(lambda e, f : e*f, energyRecHitSCEle2_recal, main_tree.fracRecHitSCEle2)))
        mustEnergySCEle_recal[1] = main_tree.mustEnergySCEle[1]*(rawEnergySCEle_recal[1]/main_tree.rawEnergySCEle[1])

        invMass_ECAL_ele_recal[0] = main_tree.invMass_ECAL_ele*sqrt(
            rawEnergySCEle_recal[0]/main_tree.rawEnergySCEle[0]*rawEnergySCEle_recal[1]/main_tree.rawEnergySCEle[1])
        
        runNumber[0] = main_tree.runNumber
        lumiBlock[0] = main_tree.lumiBlock
        eventNumber[0] = main_tree.eventNumber
        
        recal_tree.Fill()
    
    recal_tree.Write()
    recal_file.Close()

    sys.exit(0)
