#ifndef _IC_MANAGER_
#define _IC_MANAGER_

#include <tuple>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <filesystem>
#include <unordered_map>

#include <interface/json.hpp>

using json = nlohmann::json;
namespace fs = std::filesystem;

namespace recal
{
    /**
     * @class ICManager is the main class of the rECAL package.
     * It provides the interface to load/save the IC values for all channels,
     * as well as the function to compute the a new IC value based on the the new ICs 
     * and the event time.
     */   
    class ICManager
    {
    public:    
        /** @name Constructors
         */
        ///@{
        /**
         * Default constructor.
         */
        ICManager();

        /**
         * Main constructor to read existing intercalibrations.
         * @param json_path path to the calibration summary file to be loaded.
         * @param ref_time either unix time or run number of given event; if specified, only the corresponding IOV is stored in the ics_ map.
         */        
        ICManager(fs::path json_path, unsigned int ref_time = 0);
        ///@}
        
        /** @name Utils
         */
        ///@{
        /**
         * Set IC for a given channel and IOV.
         * @param ix ieta value for EB, ix for EE.
         * @param iy iphi value for EB, iy for EE.
         * @param part -1=EE-, 0=EB, +1=EE+.
         * @param ic IC value.
         * @param iov IOV begin, either unix time or run number.
         * @return returns if the IC is set correctly.
         */
        bool setIC(int ix, int iy, int part, double ic=1., unsigned int iov=0);

        /**
         * Get IC for a given channel and IOV
         * @param ix ieta value for EB, ix for EE.
         * @param iy iphi value for EB, iy for EE.
         * @param part -1=EE-, 0=EB, +1=EE+.
         * @param time either unix time or run number of current event.
         * @return returns IC value. If one between ix,iy,part and time values are invalid 
         *         the method returns 1 and prints an error message. If all inputs are valid
         *         but no IC is stored for the given channel the method returns 1 without
         *         printing any warning.
         */
        double getIC(int ix, int iy, int part, unsigned int time=0);

        /**
         * Get IC relative to the provided value for a given channel and IOV
         * @param ix ieta value for EB, ix for EE.
         * @param iy iphi value for EB, iy for EE.
         * @param part -1=EE-, 0=EB, +1=EE+.
         * @param time either unix time or run number of current event.
         * @param ic original IC value.
         * @return returns multiplicative correction factor.
         */
        double getCorrection(int ix, int iy, int part, double ic, unsigned int time=0);
        
        /**
         * Dump IC in the specified directory. This will create one txt file per IOV
         * and the summary json file.
         * @param outpath path where the ICs will be dumped.
         * @param overwrite if the directory specified by outpath exist by default
         *        the function will exit without writing, set this to true in order
         *        to write into an existing directory.
         * @return the summary json.
         */
        json dumpICs(const fs::path outpath, bool overwrite=false);

        void dumpICs(const char* outpath, bool overwrite=false) {
            dumpICs(fs::path(outpath), overwrite);
            return;
        };

        ///@}
        
    protected:
        /** @name Internal functions
         */
        ///@{
        /**
         * Validate channel
         * @param idx hasehed index.
         * @return true if channel is valid, false otherwise.
         */
        bool validateChannel(unsigned int idx);

        /**
         * Validate channel
         * @param ix ieta value for EB, ix for EE.
         * @param iy iphi value for EB, iy for EE.
         * @param part -1=EE-, 0=EB, +1=EE+.
         * @return true if channel is valid, false otherwise.
         */
        bool validateChannel(int ix, int iy, int part);

        /**
         * Validate time
         * @param time either unix time or run number of current event.
         * @return true if time belongs to any available IOV, false otherwise.
         */
        bool validateTime(unsigned int time);
        
        /**
         * Unrolled index for ECAL channels.
         * unlike the original DetId classes here we use a single index for both EB and EE.
         * EB channels come first.
         * @param ix ieta value for EB, ix for EE.
         * @param iy iphi value for EB, iy for EE.
         * @param part -1=EE-, 0=EB, +1=EE+.
         * @return continuos unsigned index.
         */
        unsigned int hashedIndex(int ix, int iy, int part);

        /**
         * Retrive ix, iy, partition from hased index.
         * @param idx hased index.
         * @return a tuple containing ix(ieta), iy(iphi), partition(-1=EE-, 0=EB, +1=EE+).
         *         If idx is invalid return (0, 0, 0).
         */
        std::tuple<int, int, int> unhashIndex(unsigned int idx);

        /**
         * Read IC values from txt file and store them in the ics_ map.
         * @param begin IOV begin run or unix time.
         * @param file_path txt file path containing the new IC values.
         */
        void readICs(unsigned int begin, std::string file_path);
        ///@}
        
    private:
        /// @name ECAL DetId range constants
        ///@{        
        static const int MAX_IETA = 85;
        static const int MAX_IPHI = 360;
        static const int MAX_HASH_EB = 2 * MAX_IPHI * MAX_IETA - 1;
        static const int IY_MAX = 100;
        static const int EE_HALF = 7324;
        static const int MAX_HASH = MAX_HASH_EB + EE_HALF*2;
        static const unsigned short kxf[2 * IY_MAX];
        static const unsigned short kdi[2 * IY_MAX];
        ///@}
        /// setup json
        json ic_info_;
        /// (ordered) list of IOV begins (either run number or unix time)
        std::vector<unsigned int> iov_begins_;
        /// IC maps per IOV
        std::unordered_map<unsigned int, std::unordered_map<unsigned int, double> > ics_;
    };
}

#endif
