#include "interface/ICManager.h"

using namespace recal;

int main(int argc, char* argv[])
{
    ICManager icman(argv[1]);

    icman.dumpICs("/tmp/recal_test/", true);
    
    return 0;
}
