#include "interface/ICManager.h"

using namespace recal;

const unsigned short ICManager::kxf[] = {
    41, 51, 41, 51, 41, 51, 36, 51, 36, 51, 26, 51, 26, 51, 26, 51, 21, 51, 21, 51, 21, 51, 21, 51, 21, 51, 16, 51, 16,
    51, 14, 51, 14, 51, 14, 51, 14, 51, 14, 51, 9,  51, 9,  51, 9,  51, 9,  51, 9,  51, 6,  51, 6,  51, 6,  51, 6,  51,
    6,  51, 6,  51, 6,  51, 6,  51, 6,  51, 6,  51, 4,  51, 4,  51, 4,  51, 4,  51, 4,  56, 1,  58, 1,  59, 1,  60, 1,
    61, 1,  61, 1,  62, 1,  62, 1,  62, 1,  62, 1,  62, 1,  62, 1,  62, 1,  62, 1,  62, 1,  62, 1,  61, 1,  61, 1,  60,
    1,  59, 1,  58, 4,  56, 4,  51, 4,  51, 4,  51, 4,  51, 6,  51, 6,  51, 6,  51, 6,  51, 6,  51, 6,  51, 6,  51, 6,
    51, 6,  51, 6,  51, 9,  51, 9,  51, 9,  51, 9,  51, 9,  51, 14, 51, 14, 51, 14, 51, 14, 51, 14, 51, 16, 51, 16, 51,
    21, 51, 21, 51, 21, 51, 21, 51, 21, 51, 26, 51, 26, 51, 26, 51, 36, 51, 36, 51, 41, 51, 41, 51, 41, 51};

const unsigned short ICManager::kdi[] = {
    0,    10,   20,   30,   40,   50,   60,   75,   90,   105,  120,  145,  170,  195,  220,  245,  270,  300,  330,
    360,  390,  420,  450,  480,  510,  540,  570,  605,  640,  675,  710,  747,  784,  821,  858,  895,  932,  969,
    1006, 1043, 1080, 1122, 1164, 1206, 1248, 1290, 1332, 1374, 1416, 1458, 1500, 1545, 1590, 1635, 1680, 1725, 1770,
    1815, 1860, 1905, 1950, 1995, 2040, 2085, 2130, 2175, 2220, 2265, 2310, 2355, 2400, 2447, 2494, 2541, 2588, 2635,
    2682, 2729, 2776, 2818, 2860, 2903, 2946, 2988, 3030, 3071, 3112, 3152, 3192, 3232, 3272, 3311, 3350, 3389, 3428,
    3467, 3506, 3545, 3584, 3623, 3662, 3701, 3740, 3779, 3818, 3857, 3896, 3935, 3974, 4013, 4052, 4092, 4132, 4172,
    4212, 4253, 4294, 4336, 4378, 4421, 4464, 4506, 4548, 4595, 4642, 4689, 4736, 4783, 4830, 4877, 4924, 4969, 5014,
    5059, 5104, 5149, 5194, 5239, 5284, 5329, 5374, 5419, 5464, 5509, 5554, 5599, 5644, 5689, 5734, 5779, 5824, 5866,
    5908, 5950, 5992, 6034, 6076, 6118, 6160, 6202, 6244, 6281, 6318, 6355, 6392, 6429, 6466, 6503, 6540, 6577, 6614,
    6649, 6684, 6719, 6754, 6784, 6814, 6844, 6874, 6904, 6934, 6964, 6994, 7024, 7054, 7079, 7104, 7129, 7154, 7179,
    7204, 7219, 7234, 7249, 7264, 7274, 7284, 7294, 7304, 7314};

//---Constructors---
ICManager::ICManager():
    ic_info_()
{
    ic_info_["IOVs"] = {};
}

ICManager::ICManager(fs::path json_path, unsigned int ref_time):
    ic_info_()
{
    std::ifstream json_file(json_path);
    json_file >> ic_info_;
    json_file.close();
    for(auto& [iov, attr] : ic_info_["IOVs"].items())
    {
	iov_begins_.push_back(attr["begin"]);
	if (ref_time == 0) readICs(attr["begin"], attr["file"]);
    }
    std::sort(iov_begins_.begin(), iov_begins_.end());

    // if ref_time specified, load only the last IOV previous to ref_time
    if (ref_time != 0){
      // get IOV from time (find the smallest IOV begin that is greater than time and take
      // the previous IOV)
      auto ref_iov = *(--std::upper_bound(iov_begins_.begin(), iov_begins_.end(), ref_time));
      for(auto& [iov, attr] : ic_info_["IOVs"].items())
	{	  
	  if (attr["begin"] == ref_iov) readICs(attr["begin"], attr["file"]);
	}     
    }
}

//---Utils---
bool ICManager::setIC(int ix, int iy, int part, double ic, unsigned int iov)
{
    // channel validity check
    auto idx = hashedIndex(ix, iy, part);
    if(idx < 0 || idx > MAX_HASH)
    {
        std::cout << ">>> ICManager::setIC --- invalid channel: ix="
                  << ix << " iy=" << iy << " partition=" << part << std::endl;
        return false;
    }

    // new IOV
    if(std::find(iov_begins_.begin(), iov_begins_.end(), iov) == iov_begins_.end())
        iov_begins_.insert(std::upper_bound(iov_begins_.begin(), iov_begins_.end(), iov), iov);

    // insert new IC value
    ics_[iov][idx] = ic;

    return true;
}

double ICManager::getIC(int ix, int iy, int part, unsigned int time)
{
    // channel validity check
    if(!validateChannel(ix, iy, part))
    {
        std::cout << ">>> ICManager::getCorrection --- Returning 1." << std::endl;
        return 1.;
    }
    
    // IOV validity check
    if(!validateTime(time))
    {
        std::cout << ">>> ICManager::getCorrection --- Returning 1." << std::endl;
        return 1.;
    }

    // get IOV from time (find the smallest IOV begin that is greater than time and take
    // the previous IOV)
    auto iov = *(--std::upper_bound(iov_begins_.begin(), iov_begins_.end(), time));
    auto& iov_ics = ics_[iov];
    auto idx = hashedIndex(ix, iy, part);
    
    // channel availability
    if(iov_ics.find(idx) == iov_ics.end())
        return 1.;
    else
        return iov_ics[idx];        
}

double ICManager::getCorrection(int ix, int iy, int part, double ic, unsigned int time)
{
    // channel validity check
    if(!validateChannel(ix, iy, part))
    {
        std::cout << ">>> ICManager::getCorrection --- Returning 1." << std::endl;
        return 1.;
    }
    
    // IOV validity check
    if(!validateTime(time))
    {
        std::cout << ">>> ICManager::getCorrection --- Returning 1." << std::endl;
        return 1.;
    }

    // get IOV from time (find the smallest IOV begin that is greater than time and take
    // the previous IOV)
    auto iov = *(--std::upper_bound(iov_begins_.begin(), iov_begins_.end(), time));
    auto& iov_ics = ics_[iov];
    auto idx = hashedIndex(ix, iy, part);
    
    // channel availability
    if(iov_ics.find(idx) == iov_ics.end())
        return 1.;
    else
        return iov_ics[idx]/ic;        
}

json ICManager::dumpICs(const fs::path outpath, bool overwrite)
{
    //---rewrite check
    if(!overwrite && fs::exists(outpath))
    {
        std::cout << ">>> ICManager::dumpICs --- path " << outpath
                  << " exist, if you want to overwrite it please specify overwrite=true" << std::endl;
        return json();
    }

    fs::create_directory(outpath);

    //---populate json
    if(ic_info_["IOVs"].size() == 0)
    {
        std::sort(iov_begins_.begin(), iov_begins_.end());
        for(unsigned int i=0; i<iov_begins_.size(); ++i)
            ic_info_["IOVs"]["IOV"+std::to_string(i)] = { {"begin", iov_begins_[i]} };
    }            

    //---looping over the IOVs and writing the single txt files
    for(auto& [iov, attr] : ic_info_["IOVs"].items())
    {
        std::string fname = iov+"_"+std::to_string(attr["begin"].get<int>())+".txt";
        std::ofstream ic_file(outpath/fname);
        auto& this_iov_ics = ics_[attr["begin"]];
        // dump ordered ICs
        for(unsigned int idx=0; idx<this_iov_ics.size(); ++idx)
        {
            auto [ix, iy, part] = unhashIndex(idx);
            ic_file << ix << " " << iy << " " << part << " " << this_iov_ics[idx] << " 0.0" << std::endl;
        }
        ic_file.close();

        // update ic_info_
        attr["file"] = fs::absolute(outpath/fname);
    }

    //---summary json file
    std::ofstream ic_info_file(outpath/"ic_info.json");
    ic_info_file << ic_info_.dump(4);
    ic_info_file.close();

    return ic_info_;    
}

//---Internal functions---
bool ICManager::validateChannel(unsigned int idx)
{
    if(idx >= 0 && idx <= MAX_HASH)
        return true;
    else
    {
        auto [ix, iy, part] = unhashIndex(idx);
        std::cout << ">>> ICManager --- invalid channel: ix="
                  << ix << " iy=" << iy << " partition=" << part << std::endl;
        return false;
    }
}

bool ICManager::validateChannel(int ix, int iy, int part)
{
    return validateChannel(hashedIndex(ix, iy, part));
}

bool ICManager::validateTime(unsigned int time)
{
    // only validate against values smaller than the first IOV
    if(time >= iov_begins_[0])
        return true;
    else
    {
        std::cout << ">>> ICManager::getIC --- invalid period: " << time
                  << ". First IOV starts at: " << iov_begins_[0] << std::endl;
        return false;
    }
}

unsigned int ICManager::hashedIndex(int ix, int iy, int part)
{
    // EB
    if(part == 0)
        return (MAX_IETA + (ix/std::abs(ix)>0 ? ix - 1 : ix)) * MAX_IPHI + iy - 1;
    // EE
    else
    {
        int jd = 2 * (iy - 1) + (ix - 1) / 50;
        return MAX_HASH_EB + ((part>0 ? EE_HALF : 0) + kdi[jd] + ix - kxf[jd]);
    }
}

std::tuple<int, int, int> ICManager::unhashIndex(const unsigned int idx)
{
    // Invilid index
    if(!validateChannel(idx))
    {
        std::cerr << ">>> ICManager::unhashIndex --- index " << idx << " out of range" << std::endl;
        return std::make_tuple(0, 0, 0);
    }
    
    // EB
    if(idx <= MAX_HASH_EB)
    {
        const int ring = idx/MAX_IPHI;
        if(ring < MAX_IETA)
            return std::make_tuple(ring-MAX_IETA, std::round((float(idx)/MAX_IPHI-ring)*360+1), 0);
        else
            return std::make_tuple(ring-MAX_IETA+1, std::round((float(idx)/MAX_IPHI-ring)*360+1), 0);
    }
    // EE
    else
    {
        auto hi = idx - MAX_HASH_EB;
        const int iz(hi < EE_HALF ? -1 : 1);
        const unsigned int di(hi % EE_HALF);
        const int ii((std::upper_bound(kdi, kdi + (2 * IY_MAX), di) - kdi) - 1);
        const int iy(1 + ii / 2);
        const int ix(kxf[ii] + di - kdi[ii]);

        return std::make_tuple(ix, iy, iz); 
    }
}

void ICManager::readICs(unsigned int begin, std::string file_path)
{
    std::ifstream ic_values(file_path);
    std::string line;
    while(std::getline(ic_values, line))
    {
        std::istringstream data(line);
        int ix, iy, part;
        double ic, err;
        try
        {
            if(!(data >> ix >> iy >> part >> ic >> err))
                throw ">>> ICManager::readICs: invalid syntax in IC file: "+line;
            else if(!validateChannel(ix, iy, part))
                throw ">>> ICManager::readICs: invalid channel: "+line;
            else
                ics_[begin][hashedIndex(ix, iy, part)] = ic;                
        }
        catch(std::string line)
        {
            std::cerr << line << std::endl;
            throw;
        }
    }          
}
