#include "interface/ICManager.h"
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

using json = nlohmann::json;

//---Python bindings
PYBIND11_MODULE(recal, m) {
    pybind11::class_<recal::ICManager>(m, "ICManager")
        .def(pybind11::init<const std::string>())
        .def(pybind11::init())
        .def("setIC", &recal::ICManager::setIC)
        .def("getIC", &recal::ICManager::getIC)
        .def("getCorrection", &recal::ICManager::getCorrection)
        .def("dumpICs", (json (recal::ICManager::*)(const fs::path , bool)) &recal::ICManager::dumpICs, 
             "Dump IC files and summary",
             pybind11::arg("outpath"),
             pybind11::arg("overwrite") = false)
        .def("dumpICs", (void (recal::ICManager::*)(const char* , bool)) &recal::ICManager::dumpICs,
             "Dump IC files and summary",
             pybind11::arg("outpath"),
             pybind11::arg("overwrite") = false);
}
